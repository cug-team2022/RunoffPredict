
<!-- README.md is generated from README.Rmd. Please edit that file -->

# 1 RunoffPredict

<!-- badges: start -->
<!-- badges: end -->

    #> ℹ Loading RunoffPredict
    #> Loading required package: data.table
    #> Loading required package: dplyr
    #> 
    #> Attaching package: 'dplyr'
    #> The following objects are masked from 'package:data.table':
    #> 
    #>     between, first, last
    #> The following objects are masked from 'package:stats':
    #> 
    #>     filter, lag
    #> The following objects are masked from 'package:base':
    #> 
    #>     intersect, setdiff, setequal, union
    #> Loading required package: purrr
    #> 
    #> Attaching package: 'purrr'
    #> The following object is masked from 'package:data.table':
    #> 
    #>     transpose
    #> Loading required package: lubridate
    #> 
    #> Attaching package: 'lubridate'
    #> The following objects are masked from 'package:data.table':
    #> 
    #>     hour, isoweek, mday, minute, month, quarter, second, wday, week,
    #>     yday, year
    #> The following objects are masked from 'package:base':
    #> 
    #>     date, intersect, setdiff, union
    #> Registered S3 method overwritten by 'RunoffPredict':
    #>   method         from  
    #>   predict.ranger ranger

## 1.1 数据

### 1.1.1 赛方数据

#### 1.1.1.1 降水

``` r
head(df_prcp)
#>                   date 80836740 80838140 80838940 80840040 80828140 80828740
#> 1: 2006-06-04 00:00:00       NA       NA       NA       NA   4.9375       NA
#> 2: 2006-06-04 01:00:00       NA       NA       NA       NA   4.9375       NA
#> 3: 2006-06-04 02:00:00       NA       NA       NA       NA   4.9375       NA
#> 4: 2006-06-04 03:00:00       NA       NA       NA       NA   4.9375       NA
#> 5: 2006-06-04 04:00:00       NA       NA       NA       NA   4.9375       NA
#> 6: 2006-06-04 05:00:00       NA       NA       NA       NA   4.9375       NA
#>    80829040 80829740 80830140 80841540 80843940 80844340 80847140
#> 1:       NA       NA       NA       NA       NA       NA       NA
#> 2:       NA       NA       NA       NA       NA       NA       NA
#> 3:       NA       NA       NA       NA       NA       NA       NA
#> 4:       NA       NA       NA       NA       NA       NA       NA
#> 5:       NA       NA       NA       NA       NA       NA       NA
#> 6:       NA       NA       NA       NA       NA       NA       NA
```

对应的站点信息

``` r
head(st)
#>        site  name            geometry
#> 1: 80828880 st_01  597254.6,2654801.8
#> 2: 80828890 st_02  592203.8,2654901.9
#> 3: 80841540 st_03  600167.5,2606119.3
#> 4: 80838140 st_04  602327.9,2654335.5
#> 5: 80828900 st_05  570514.9,2611565.7
#> 6: 80838940 st_06  589900.2,2636941.8
```

#### 1.1.1.2 径流数据

#### 1.1.1.3 ET数据

### 1.1.2 自筹数据

#### 1.1.2.1 ERA5L

``` r
head(df_ERA5L)
#>                   date          ro          e          tp        pev       t2m
#> 1: 2006-01-01 00:00:00          NA         NA          NA         NA  8.314586
#> 2: 2006-01-01 01:00:00 0.002566611 0.03172038 0.001097134 0.05959371  9.046857
#> 3: 2006-01-01 02:00:00 0.004389298 0.06769074 0.001499441 0.12417163 10.008387
#> 4: 2006-01-01 03:00:00 0.004608085 0.12480355 0.006504350 0.22200602 11.442267
#> 5: 2006-01-01 04:00:00 0.003160385 0.20841288 0.002762956 0.36915091 13.225156
#> 6: 2006-01-01 05:00:00 0.003154306 0.24391578 0.000000000 0.44534782 14.462653
```

### 1.1.3 最终的训练数据

-   流域面积为`2561.219`![km^2](https://latex.codecogs.com/png.image?%5Cdpi%7B110%7D&space;%5Cbg_white&space;km%5E2 "km^2")，借助流域面积将Q(![m^3/s](https://latex.codecogs.com/png.image?%5Cdpi%7B110%7D&space;%5Cbg_white&space;m%5E3%2Fs "m^3/s"))转为R(![mm/h](https://latex.codecogs.com/png.image?%5Cdpi%7B110%7D&space;%5Cbg_white&space;mm%2Fh "mm/h"))。

-   前4个站点在流域内：`c("80836740", "80838140", "80838940", "80840040")`；紧追其后的3个站点在流域外：`c("80841540", "80843940", "80844340")`；余下的6个站点无站点信息。

-   PET为赛方提供的ET数据，估计是潜在蒸散发数据。

#### 1.1.3.1 ERA5L数据说明

-   `t2m`: 2米空气温度(℃)
-   `e`: 实际蒸发（mm/h）
-   `ro`: 模拟的总径流深（mm/h），**可以考虑把该变量加入TCN网络**

#### 1.1.3.2 降水数据说明

-   修复了OBS_prcp，数据不匹配时，以daily数据为主。

-   提供了多种降水数据Prcp（mm/h）。除了赛方提供的降水数据，我们采用了2套再分析资料数据（ERA5L和CMFD），2套高精度的卫星数据（GSMap和GPM）。降水的单位均为：mm/h。`CMFD`原始为3-hourly，将其均分到了逐小时。

``` r
head(dataset_GuiJiang)
#>                   date  R  Q 80836740 80838140 80838940 80840040 80841540
#> 1: 2006-04-10 00:00:00 NA NA       NA       NA       NA       NA       NA
#> 2: 2006-04-10 01:00:00 NA NA       NA       NA       NA       NA       NA
#> 3: 2006-04-10 02:00:00 NA NA       NA       NA       NA       NA       NA
#> 4: 2006-04-10 03:00:00 NA NA       NA       NA       NA       NA       NA
#> 5: 2006-04-10 04:00:00 NA NA       NA       NA       NA       NA       NA
#> 6: 2006-04-10 05:00:00 NA NA       NA       NA       NA       NA       NA
#>    80843940 80844340 80828140 80828740 80829040 80829740 80830140 80847140
#> 1:       NA       NA       NA       NA       NA       NA       NA       NA
#> 2:       NA       NA       NA       NA       NA       NA       NA       NA
#> 3:       NA       NA       NA       NA       NA       NA       NA       NA
#> 4:       NA       NA       NA       NA       NA       NA       NA       NA
#> 5:       NA       NA       NA       NA       NA       NA       NA       NA
#> 6:       NA       NA       NA       NA       NA       NA       NA       NA
#>         t2m       ro          e prcp_ERA5L  prcp_GSMap prcp_GPM   prcp_CMFD
#> 1: 18.88907 4.058070 0.01789592 3.81993904 0.005803466        0 0.457235421
#> 2: 18.90983 5.265755 0.04163035 5.75433236 0.005917726        0 0.457235421
#> 3: 19.27153 3.611141 0.11288994 1.93903227 0.005932770        0 0.457235421
#> 4: 20.67629 3.023128 0.22525063 0.05935985 0.005939487        0 0.009319559
#> 5: 22.82307 3.191180 0.36802301 0.06328455 0.005939488        0 0.009319559
#> 6: 24.06922 3.201009 0.35243352 0.37521947 0.005932778        0 0.009319559
#>           PET  PET_ERA5L
#> 1: 0.02788381 0.01795255
#> 2: 0.10359089 0.04174368
#> 3: 0.20990446 0.11288872
#> 4: 0.28041287 0.23175624
#> 5: 0.33970054 0.44332701
#> 6: 0.36872842 0.51425895
```

# 2 研究结果

## 2.1 1. 数据选取–降水与潜在蒸散发资料挑选

### 2.1.1 (a)变量说明

`prcp1`:
4站点平均。在流域内的4个站点prcp平均：`c("80836740", "80838140", "80838940", "80840040")`
`prcp2`: 7站点平均。除流域内4站点，外加在流域外的3站点（c(“80841540”,
“80843940”, “80844340”)） `prcp3`: 13站点平均。

<img src="image/README/image-20220530045657582.png" alt="image-20220530045657582" style="zoom:50%;" />

结果显示：

降水资料中：应该采用13站点平均作为输入。站点降水资料(r=0.25)优于再分析资料(r
= 0.21)与卫星资料(r = 0.2)
再分析资料中，CMFD质量优于ERA5L和另外两种卫星降水产品，可以作为候补降水产品。

PET资料中：自带的PET数据质量较好，因此不采用ERA5L的版本。

> 但是**2012年PET资料缺失**，需要进行插补？

## 2.2 2. 前期分析

### 2.2.1 降水径流滞后时间分析

根据上一步中质量较好的prcp3和CMFD，作降水径流滞后时间分析。

### 2.2.2 洪水特征分析

持续时间、发生月份、频次

200806降水过程分析

### 2.2.3 统计方法–建模
