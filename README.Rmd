---
output: 
  github_document:
    number_sections: true
---

<!-- README.md is generated from README.Rmd. Please edit that file -->

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  fig.path = "man/figures/README-",
  out.width = "100%"
)
```

# RunoffPredict

<!-- badges: start -->
<!-- badges: end -->

```{r example, echo=FALSE}
devtools::load_all(".")
# library(RunoffPredict)
## basic example code
```

## 数据

### 赛方数据

#### 降水
```{r}
head(df_prcp)
```

对应的站点信息
```{r}
head(st)
```

#### 径流数据

#### ET数据

### 自筹数据

#### ERA5L

```{r}
head(df_ERA5L)
```

### 最终的训练数据

- 流域面积为`2561.219`$km^2$，借助流域面积将Q($m^3/s$)转为R($mm/h$)。

- 前4个站点在流域内：`c("80836740", "80838140", "80838940", "80840040")`；紧追其后的3个站点在流域外：`c("80841540", "80843940", "80844340")`；余下的6个站点无站点信息。

- PET为赛方提供的ET数据，估计是潜在蒸散发数据。

#### ERA5L数据说明

- `t2m`: 2米空气温度(℃)
- `e`: 实际蒸发（mm/h）
- `ro`: 模拟的总径流深（mm/h），**可以考虑把该变量加入TCN网络**

#### 降水数据说明

- 修复了OBS_prcp，数据不匹配时，以daily数据为主。

- 提供了多种降水数据Prcp（mm/h）。除了赛方提供的降水数据，我们采用了2套再分析资料数据（ERA5L和CMFD），2套高精度的卫星数据（GSMap和GPM）。降水的单位均为：mm/h。`CMFD`原始为3-hourly，将其均分到了逐小时。

```{r}
head(dataset_GuiJiang)
```

# 研究结果

## 1. 数据选取--降水与潜在蒸散发资料挑选

### (a)变量说明

`prcp1`: 4站点平均。在流域内的4个站点prcp平均：`c("80836740", "80838140", "80838940", "80840040")`
`prcp2`: 7站点平均。除流域内4站点，外加在流域外的3站点（c("80841540", "80843940", "80844340")）
`prcp3`: 13站点平均。

<img src="image/README/image-20220530045657582.png" alt="image-20220530045657582" style="zoom:50%;" />

结果显示：

降水资料中：应该采用13站点平均作为输入。站点降水资料(r=0.25)优于再分析资料(r = 0.21)与卫星资料(r = 0.2)
再分析资料中，CMFD质量优于ERA5L和另外两种卫星降水产品，可以作为候补降水产品。

PET资料中：自带的PET数据质量较好，因此不采用ERA5L的版本。

> 但是**2012年PET资料缺失**，需要进行插补？


## 2. 前期分析

### 降水径流滞后时间分析

根据上一步中质量较好的prcp3和CMFD，作降水径流滞后时间分析。


### 洪水特征分析

持续时间、发生月份、频次

200806降水过程分析

### 统计方法--建模

